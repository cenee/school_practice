# Saturnin README #

### This repository is my student book for my school projects ###

* I collected here a couple of projects presenting my works from very beginning (see labo02 - labo07).
* There is website address inside each of labo's located on my school's server to see how it all works. Available to see through sub-sites.

###labo02 
- is very beginning some fundamental info about HTML

[Zadanie 1](http://inet132.website.net.ii.pwr.wroc.pl/labo02/zad1/index.html)

[Zadanie 2](http://inet132.website.net.ii.pwr.wroc.pl/labo02/zad2/index.html)

[Zadanie 3](http://inet132.website.net.ii.pwr.wroc.pl/labo02/zad3/index.html)

***

###labo03  
- presenting simple attributes like 'lang', 'spellcheck' or hotkeys 'accesskey' and some

[Zadanie 1](http://inet132.website.net.ii.pwr.wroc.pl/labo03/zad1/index.html)

[Zadanie 2](http://inet132.website.net.ii.pwr.wroc.pl/labo03/zad2/index.html)

***

###labo04  
- showing how to use 'meta' data, external/internal links, basic 'text-style', quotes and lists types

[Zadanie 1](http://inet132.website.net.ii.pwr.wroc.pl/labo04/zad1/index.html)

***

###labo05  
- website arrangement: sections, articles, aside, tabls, basic gallery and form

[Zadanie 1](http://inet132.website.net.ii.pwr.wroc.pl/labo05/zad1/index.html)

***

###labo06  
- more details about tables and forms, 
- posting things on website: music, video, flash animation, frame with other page content or pdf document

[Zadanie 1](http://inet132.website.net.ii.pwr.wroc.pl/labo06/zad1/index.html)

***

###labo07 
- adding canvas content, 
- css classes and id interaction, 
- inheritance, 
- element+element it means element which is directly next after, 
- element with specific attribut and value etc

[Zadanie 1](http://inet132.website.net.ii.pwr.wroc.pl/labo07/zad1/index.html)

***