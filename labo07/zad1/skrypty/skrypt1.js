// Funkcja przechowuje informacje o pojedynyczm okręgu.
function Circle(x, y, radius, color) { 
	this.x = x;
	this.y = y;
	this.radius = radius;
	this.color = color;
	this.isSelected = false;
}
// W tej tablicy zapisuje się wszystkie narysowane na płótnie koła.
var circles = [];
var canvas;
var context;
// obsługa zdarzenia ‘load’ – nowość!
window.onload = function () {
canvas = document.getElementById("canvas");
context = canvas.getContext("2d");
canvas.onmousedown = canvasClick; 
canvas.onmouseup = stopDragging; 
canvas.onmouseout = stopDragging; 
canvas.onmousemove = dragCircle;
startDrawing(); 

// Nadaje kołu losowy rozmiar i położenie na płótnie. var radius = randomFromTo(10, 60);
var x = randomFromTo(0, canvas.width);
var y = randomFromTo(0, canvas.height);
// Nadaje kołu losową barwę.
var colors = ["green", "blue", "red", "yellow", "magenta", "orange", "brown", "purple", "pink"];
var color = colors[randomFromTo(0, 8)];

// Tworzy nowe koło.
var circle = new Circle(x, y, radius, color); // Zapisuje koło w tablicy.
circles.push(circle);
// Ponownie wypełnia płótno.
drawCircles(); }
function clearCanvas() {
// Usuwa wszystkie koła z tablicy. circles = [];
// Uaktualnia płótno.
drawCircles(); }
function drawCircles() {
// Czyści płótno.
context.clearRect(0, 0, canvas.width, canvas.height);
// Przechodzie przez wszystkie zapisane koła.
for (var i = 0; i < circles.length; i++) { 
		var circle = circles[i];
	// Rysuje wybrane koło.
	context.globalAlpha = 0.85;
	context.beginPath();
	context.arc(circle.x, circle.y, circle.radius, 0, Math.PI * 2);
	context.fillStyle = circle.color;
	context.strokeStyle = "black";
	if (circle.isSelected) { context.lineWidth = 5;
	}
	else {
	context.lineWidth = 1; 
	}
	context.fill();
	context.stroke(); 
	}
}
var previousSelectedCircle;
function canvasClick(e) {
// Odczytuje współrzędne punktu klikniętego przez użytkownika. var clickX = e.pageX - canvas.offsetLeft;
var clickY = e.pageY - canvas.offsetTop;
// Sprawdza, które z kół zostało kliknięte.
for (var i = circles.length - 1; i >= 0; i--) { 
		var circle = circles[i];
	var distanceFromCenter = Math.sqrt(Math.pow(circle.x - clickX, 2) 
		+ Math.pow(circle.y - clickY, 2)) if (distanceFromCenter <= circle.radius) {
			if (previousSelectedCircle != null) previousSelectedCircle.isSelected = false;
			 	previousSelectedCircle = circle;

	circle.isSelected = true;
	// Pozwala funkcji dragCircle na przeciągnięcie koła.
	isDragging = true;
	drawCircles();
	return; }
	} 
}
var isDragging = false;
function stopDragging() {
	isDragging = false;
}
function dragCircle(e) {
// Czy koło jest przeciągane?
	 if (isDragging == true) {
	// Na wszelki wypadek sprawdza, czy aby na pewno przeciąganym obiektem jest koło.
		if (previousSelectedCircle != null) {
		// Znajduje nowe współrzędne kursora myszy. 
		var x = e.pageX - canvas.offsetLeft;
		var y = e.pageY - canvas.offsetTop;
		// Przesuwa koło w to miejsce.
		previousSelectedCircle.x = x;
		 previousSelectedCircle.y = y;
		// Uaktualnia płótno.
		drawCircles();
		}
	}
}
function randomFromTo(from, to) {
return Math.floor(Math.random() * (to - from + 1) + from);
}
