var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var kulki = [new Kulka(), new Kulka(), new Kulka(), new Kulka(),new Kulka()];


function Kulka(){
    this.x = canvas.width*Math.random();
    this.y = canvas.height*Math.random();
    this.r = 15;
    this.color = 'rgba('+Math.floor(Math.random()*256)+','+Math.floor(Math.random()*256)+','+Math.floor(Math.random()*256)+',0.5)';
    
    this.rysuj = function(){
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.r, 0,2*Math.PI);
        ctx.fillStyle = this.color;
        ctx.fill();
    };
    
        
    this.ruchLewo = function(){
        this.x += -10;
    };
    this.ruchPrawo = function(){
        this.x += 10;
    };
    this.ruchGora = function(){
        this.y += -10;    
    };
    this.ruchDol = function(){
        this.y += 10;    
    };
    
    
}

function ruch(e){

    if (e.keyCode == 65 || e.keyCode == 37) {
        kulki.forEach(function(kulka){
            kulka.ruchLewo();
            kulka.rysuj();
        });
        /*for(var i = 0; i < kulki.lenght; i++){
            kulki[i].ruchLewo();
        }*/
    }else if (e.keyCode == 68 || e.keyCode == 39) {
        kulki.forEach(function(kulka){
            kulka.ruchPrawo();
            kulka.rysuj();
        });
        
    }else if (e.keyCode == 87 || e.keyCode == 38) {
        kulki.forEach(function(kulka){
            kulka.ruchGora();
            kulka.rysuj();
        });
        
    }else if (e.keyCode == 83 || e.keyCode == 40) {
        kulki.forEach(function(kulka){
            kulka.ruchDol();
            kulka.rysuj();
        });
    }
}


    
    


window.onkeydown = ruch;